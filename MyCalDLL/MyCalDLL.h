#ifndef __MYCALDLL__h
#define __MYCALDLL__h

#ifdef MYCALDLL_EXPORTS
#define MYCALDLL_API __declspec(dllexport)
#else
#define MYCALDLL_API __declspec(dllimport)
#endif

MYCALDLL_API int Addition(int num1, int num2);

#endif