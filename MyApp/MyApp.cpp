#include<Windows.h>
#include<iostream>
using namespace std;

typedef int(_cdecl *FunAdd)(int a, int b);//redefine functions //funadd is function pointer
int main()
{
	//local variable

	HMODULE hModule;
	//Load library - get handle to DLL module
	hModule = LoadLibrary(TEXT("C:\\Users\\shwetha.b\\Documents\\Visual Studio 2013\\Projects\\MyMath\\Debug\\MyMath.dll"));
	if (NULL == hModule)
	{
		cout << "Load Library Failed & Error no. " << GetLastError() << endl;
	}
	cout << "Load Library Success";
	//Get procAddress-Get the function address
	FunAdd AdditionFun = (FunAdd)GetProcAddress(hModule, "Addition");
	//check function address is valid or not
	if (NULL == AdditionFun)
	{
		cout << "Function address is invalid & Error no. " << GetLastError() << endl;
	}
	cout << "Addition"<<AdditionFun(10,10);

	//free DLL module
	FreeLibrary(hModule);
	system("pause");
	return 0;
}