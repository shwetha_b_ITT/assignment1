#include<Windows.h>

#ifdef __cplusplus
extern "C"{ //export c interface for C++ code
#endif
	//definition function
	_declspec(dllexport) int __cdecl Addition(int num1, int num2)
	{
		int sum;
		sum = num1 + num2;
		return sum;
	}
#ifdef __cplusplus
}
#endif